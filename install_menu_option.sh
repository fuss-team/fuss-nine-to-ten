#!/bin/bash -e
# This file is part of the FUSS FUCC in-place upgrade project.
# Copyright (C) 2021 The FUSS Project <info@fuss.bz.it>
# Author: Marco Marinello <contact-nohuman@marinello.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

FETCH=$(cat /srv/tftp/pxelinux.cfg/default|grep APPEND|tail -1|rev|cut -d ' ' -f 1|rev)

if ! grep "label in-place-upgrade" /srv/tftp/pxelinux.cfg/default; then
  cat >> /srv/tftp/pxelinux.cfg/default <<EOF
label in-place-upgrade
MENU LABEL FUSS 9 to 10 in-place upgrade
# Please remove the ssh key from .ssh/authorized_keys of the clonezilla
# user instead of using this line!
# MENU PASSWD myPassword
KERNEL live/vmlinuz
APPEND initrd=live/initrd.img boot=live username=clonezilla union=overlay config components quiet noswap edd=on nomodeset nodmraid locales= keyboard-layouts=NONE ocs_live_batch=no net.ifnames=0 nosplash noprompt keyboard-layouts=it locales=it_IT.UTF-8 ocs_prerun1="sshfs clonezilla@proxy:/srv/clonezilla/ /home/partimag -o IdentityFile=/home/clonezilla/.ssh/id_rsa -o StrictHostKeyChecking=no" ocs_prerun2="screen -S XY '/home/partimag/fuss-nine-to-ten/deploy.sh' " $FETCH
EOF
  echo "Done."
else
    echo "Entry present. Not altering."
fi
