#!/bin/bash -e
# This file is part of the FUSS FUCC in-place upgrade project.
# Place this script in OctoNet in order to automatically update FUSS 9 clients
# Copyright (C) 2021 The FUSS Project <info@fuss.bz.it>
# Author: Marco Marinello <contact-nohuman@marinello.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

if [ "$(lsb_release -sc)" == "stretch" ]; then
  cd /root
  sed -i '/^mozilla\/DST_Root_CA_X3/s/^/!/' /etc/ca-certificates.conf && update-ca-certificates -f
  wget https://gitlab.fuss.bz.it/fuss-team/fuss-nine-to-ten/-/raw/main/run_upgrade.sh
  wget https://gitlab.fuss.bz.it/fuss-team/fuss-nine-to-ten/-/raw/main/upgrade.yml
  wget -O /etc/rc.local https://gitlab.fuss.bz.it/fuss-team/fuss-nine-to-ten/-/raw/main/rc.local
  chmod +x /etc/rc.local /root/run_upgrade.sh
  sed -i "s/1/0/g" /etc/apt/apt.conf.d/20auto-upgrades
  # Comment next line to avoid PC reboot
  systemctl reboot
fi

exit 0
