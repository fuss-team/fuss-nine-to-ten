# FUSS 9 to 10
## In-place upgrade of FUSS Clients

### Quickstart

On the FUSS Server

```bash
cd /srv/clonezilla
git clone https://gitlab.fuss.bz.it/fuss-team/fuss-nine-to-ten.git
cd fuss-nine-to-ten
./install_menu_option.sh
```

and you're ready to go. Boot from LAN a PC and you'll see the new "FUSS 9 to 10 in-place upgrade" option.


### Packages caching

Always on the FUSS Server, install the `squid-deb-proxy` package:

```bash
apt install -qq -y squid-deb-proxy
```

then add a few more repositories to be cached:
```bash
cat > /etc/squid-deb-proxy/mirror-dstdomain.acl.d/20-fuss <<EOF
archive.fuss.bz.it
dl.google.com
download.videolan.org
.ubuntu.com
EOF
systemctl reload squid-deb-proxy
```

finally, add the custom configuration for the proxy in FUSS nine-to-ten files:
```bash
cat > /srv/clonezilla/fuss-nine-to-ten/apt-custom-proxy.conf <<EOF
Acquire::http::proxy "http://SER.VE.R.IP:8000";
Acquire::https::proxy "http://SER.VE.R.IP:8000";
EOF
```

**IMPORTANT**: if you don't have 40GB of free space in the root of the server, make sure to edit the line 
```
cache_dir aufs /var/cache/squid-deb-proxy 40000 16 256
```

in `/etc/squid-deb-proxy/squid-deb-proxy.conf`, replacing `40000` with an appropriate value.


### Launching from OctoNet / OctoFUSS Client

You can use the "Script" feature of OctoNet to trigger an update to a client. To do that,
just copy the content of the `octonet_script.sh` file into a new script and proceed as you would do normally.

`gitlab.fuss.bz.it` is whitelisted from the proxy in Debian 10 but make sure it is also in your setup. You can simply
launch `wget -O - https://gitlab.fuss.bz.it/fuss-team/fuss-nine-to-ten/-/raw/main/run_upgrade.sh` on a client
and check the output to confirm it's working.
