#!/bin/bash
# This file is part of the FUSS FUCC in-place upgrade project.
# Copyright (C) 2021 The FUSS Project <info@fuss.bz.it>
# Author: Marco Marinello <contact-nohuman@marinello.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

if [ "$1" == "run" ]; then
  LOGFILE="/root/9to10.log"
  STATEFILE="/root/upgraded_9to10"
  cat <<EOF

FUSS 9 to 10 in-place upgrade tool

Now starting...

EOF
  if [ "$(lsb_release -sc)" == "buster" ] && [ -e "$STATEFILE" ]; then
    echo "[FUSS] Final phase, upgrading configuration"
    echo "[FUSS] Run dpkg --configure -a"
    dpkg --configure -a
    echo "[FUSS] Run fuss-client -U"
    fuss-client -U
    echo "[FUSS] Cleaning up..."
    rm /etc/rc.local
    rm /root/run_upgrade.sh
    rm /root/upgrade.yml
    sed -i "s/0/1/g" /etc/apt/apt.conf.d/20auto-upgrades
    systemctl reboot
    exit 0
  fi
  if [ "$(lsb_release -sc)" == "buster" ]; then
    echo "Only upgrading to buster from stretch is currently supported"
    rm /etc/rc.local
    rm /root/run_upgrade.sh
    rm /root/upgrade.yml
    sed -i "s/0/1/g" /etc/apt/apt.conf.d/20auto-upgrades
    systemctl reboot
    exit 1
  fi
  echo "[FUSS] Removing /var/lib/dpkg/statoverride"
  rm -f /var/lib/dpkg/statoverride
  echo "[FUSS] Setting DEBIAN_FRONTEND=noninteractive"
  export DEBIAN_FRONTEND=noninteractive
  echo "[FUSS] Run dpkg --configure -a"
  dpkg --configure -a || echo "$(date) Pre upgrade dpkg --configure -a unsuccessful" >> $LOGFILE
  echo "[FUSS] apt install -f -qq -y"
  apt install -f -qq -y || echo "$(date) Pre upgrade apt install -f -qq -y unsuccessful" >> $LOGFILE
  echo "[FUSS] Run apt update -y"
  apt update -y || echo "$(date) Pre upgrade apt update -y unsuccessful" >> $LOGFILE
  if ! apt-cache policy ansible | grep backports > /dev/null; then
    echo "[FUSS] Backports not enabled, enabling..."
    echo "deb http://ftp.de.debian.org/debian/ $(lsb_release -sc)-backports main contrib non-free" > /etc/apt/sources.list.d/backports.list
    apt update -y
    echo "[FUSS] Backports enabled"
  fi
  echo "[FUSS] Install ansible from backports"
  apt install -t $(lsb_release -sc)-backports -qq -y ansible || echo "$(date) Install ansible from backports unsuccessful" >> $LOGFILE
  echo "[FUSS] Launch upgrade playbook"
  ansible-playbook -c local -i localhost, /root/upgrade.yml || echo "$(date) Upgrade playbook unsuccessful" >> $LOGFILE
  echo "[FUSS] Run dpkg --configure -a"
  dpkg --configure -a || echo "$(date) Post upgrade dpkg --configure -a unsuccessful" >> $LOGFILE
  echo "[FUSS] Run apt clean"
  apt clean
  echo "[FUSS] Run apt update -y"
  apt update -y || echo "$(date) Post upgrade apt update -y unsuccessful" >> $LOGFILE
  echo "[FUSS] Run apt full-upgrade -qq -y"
  apt full-upgrade -qq -y || echo "$(date) Post upgrade apt full-upgrade -qq -y unsuccessful" >> $LOGFILE
  echo "[FUSS] Cleanup current proxy configuration"
  echo -n > /etc/apt/apt.conf
  echo "[FUSS] All done, rebooting"
  sleep 10
  date > $STATEFILE
  systemctl reboot
  exit 0
fi

sleep 5
systemctl stop lightdm

while ps -C apt-get,apt,dpkg > /dev/null ; do
  echo "APT running ... wait for it to finish" >> /dev/tty7
	sleep 10
done

while ! ping -c 1 proxy; do
	echo "No ping to proxy... wait 2 more seconds"
	sleep 2
done

screen -d -m -S XY
screen -S XY -X stuff "exec 2>&1 > /dev/tty7\n"
screen -S XY -X stuff "/root/run_upgrade.sh run \n"

exit 0
