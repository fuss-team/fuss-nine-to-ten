#!/bin/bash
# This file is part of the FUSS FUCC in-place upgrade project.
# Copyright (C) 2021 The FUSS Project <info@fuss.bz.it>
# Author: Marco Marinello <contact-nohuman@marinello.bz.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# From Donato's script
getDisk(){
    myDevice="$(lsblk -l -n | grep disk | awk '{print $1}')"

    if [ $(echo $myDevice | wc -w) -gt 1 ];then
        echo "ATTENZIONE! Questo computer sembra provvisto di più di un disco!"
        echo ""
        echo "La procedura automatica non prevede l'installazione su sistemi"
        echo "multidisco. Contattare il tecnico informatico."
        echo ""
        echo "Per sicurezza il computer verrà spento tra 10 secondi."
    	sleep 10
    	shutdown -h now
    else
        diskVar="$(echo $myDevice | tr -d [:blank:])"
    fi
}

getRootPartitionFuss(){
    for i in $(lsblk -lp | grep part | awk '{print $1}')
    do
      mount $i /mnt
      if [ -d "/mnt/etc/fuss-client" ];then
          fussDir="$i"
      fi
      umount /mnt
    done
    echo "$fussDir"
}

getDisk
mountPoint="/mnt"
rootPartition=$(getRootPartitionFuss)
mount $rootPartition $mountPoint
# End from Donato's script

cp /home/partimag/fuss-nine-to-ten/run_upgrade.sh $mountPoint/root/
cp /home/partimag/fuss-nine-to-ten/upgrade.yml $mountPoint/root/
cp /home/partimag/fuss-nine-to-ten/rc.local $mountPoint/etc/
# Disable unattended upgrades
sed -i "s/1/0/g" $mountPoint/etc/apt/apt.conf.d/20auto-upgrades
# Configure a custom proxy if required (for caching)
PROXY_FILE="/home/partimag/fuss-nine-to-ten/apt-custom-proxy.conf"
[ -e "$PROXY_FILE" ] && cat "$PROXY_FILE" > $mountPoint/etc/apt/apt.conf

reboot
